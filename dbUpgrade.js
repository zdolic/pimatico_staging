var path = require('path');
var models = require('./common/models');
var dbConfig = require("./dbConfig.js");

var migrationCmd = "sudo sequelize db:migrate --migrations-path migrations/migrations --config migrations/config.json";

var migrationSecondary = "sudo node_modules/.bin/sequelize db:migrate --migrations-path migrations/migrations --config migrations/config.json"

exec(migrationCmd, function(err, std, sterr){
	if(err===null){
		console.log(chalk.red("Migration succesefull..."));
	} else {
		console.log(chalk.red("Fallback. Trying again with secondary sequelize cli path"));

		exec(migrationSecondary, function(err2,std2, sterr){
			if(err2===null){
				console.log(chalk.red("Migration succesefull..."));
			} else {
				console.log(chalk.red("Migration unsucceseful."));
			}
		})
	}
});