var path = require('path');
var _ = require("lodash");

var config = {
	//database
	models_path: path.join(__dirname, "./common/models"),
	dialect: "sqlite",
	storagePath: _.isUndefined(process.env.DOCKER_PIMATICO_INSTANCE)===true ?  path.join(__dirname, "./common/db/database.sqlite") : path.join(__dirname, "./data/db/database.sqlite"),
	//default login username:
	name: "admin",
	email: "admin@admin.com",
	password: "admin"
};


module.exports = config;