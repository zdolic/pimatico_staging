var dbConfig = require("./dbConfig.js");
var exec = require('child_process').exec;
var chalk = require('chalk');
var models = require('./common/models');
var dbDefaults = require('./common/db/dbDefaults');

var cmd = "sqlite3 "+dbConfig.storagePath + " .exit";
var removeCmd = "rm "+dbConfig.storagePath;


exec(removeCmd, function(err, std, sterr){
	if(err===null){
		console.log(chalk.red("DB removed"));
	}

	console.log(chalk.blue("Creating DB"));
	exec(cmd, function(error, stdout, stderr) {
		if(error===null){
			console.log(chalk.red("DB created"));

			models.sequelize.sync().then(function(){
				dbDefaults.createDbDefaults().then(()=>{
					console.log(chalk.red("Default user created. email: admin@admin.com | pass: admin"));
					require("./common/controllers/settings/settingsFromJSONData.js").initDbSettingBuild().then(()=>{
						models.sequelize.close();
					}); //consider some better solutions.... 
					
					
				});				
			});
		}
	});
});


