# Pull base image
FROM zdolic/rpi-pimatico-base
MAINTAINER Zrinko Dolic <zdolic@gmail.com>

ENV DOCKER_PIMATICO_INSTANCE=true

# Create app directory & Define working directory
RUN mkdir -p /usr/pimatico
VOLUME /usr/pimatico/data/db
WORKDIR /usr/pimatico

# Bundle app source and binaries
COPY . /usr/pimatico
RUN node /usr/pimatico/createDb.js

# Define default command
#CMD ["bash"]
CMD ["npm", "start"]

EXPOSE 80