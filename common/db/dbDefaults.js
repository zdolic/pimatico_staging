var crypto = require('crypto');
var db = require('../models');
var chalk = require('chalk');
var dbConfig = require('../../dbConfig');
var Promise = require("bluebird");
//var config = require('../../config.js');

var createDbDefaults = function(){
	return createDefaultUser();
};

var createDefaultUser = function(){

	return new Promise(function(resolve, reject){

		var salt = makeSalt();
		var hashedPassword = encryptPassword(dbConfig.password, salt);
		db.User.findOrCreate( {where: {id: 1}, defaults: {name: dbConfig.name ,email: dbConfig.email, salt:salt, hashed_password:hashedPassword}})
		.spread(function(createdUser, created){
			//console.log(chalk.blue("DEFAULT USER CREATED:",createdUser.get({plain:true})));
			db.Room.findOrCreate( {where: {id: 1}, defaults: {name: "Default group", createdById:1 }})
			.spread(function(createdUser, createdRoom){
				console.log(chalk.blue("Default data created."));
				//console.log(chalk.blue("DEFAULT USER CREATED:",createdUser.get({plain:true})));
				resolve(true);

				/*
				db.MasterNotification.findOrCreate(
				{
					where: {id: 1}, 
					defaults: {title: "Neki naslov", message: "Neka poruka", key: "NEKI KEY", color: "blue" }
				}).spread(function(createdObj, createdMasterNotification){
					console.log("default start message created.....");
				});
				*/

			});
		});

	});
	


};

var encryptPassword = function(password, salt) {
	if (!password) {return '';}
	return crypto.createHmac('sha1', salt).update(password).digest('hex');
};

var makeSalt = function() {
	return Math.round((new Date().valueOf() * Math.random())) + '';
};

module.exports.createDbDefaults = createDbDefaults;