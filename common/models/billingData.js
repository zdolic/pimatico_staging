
module.exports = function(sequelize, DataTypes) {
	var BillingData = sequelize.define('BillingData', {
	quantity: DataTypes.FLOAT,

  unit: DataTypes.STRING(10),
  unitPrice: DataTypes.FLOAT,
  totalPrice: DataTypes.FLOAT,
  status: { //0-closed, 1-open
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  }, 
  optData: DataTypes.STRING(500)  //json type but with 500 leters limit for optional inputs later on...
  }, 
  {
    classMethods: {
      associate: function(models) {
        BillingData.belongsTo(models.BillingTicket, { onDelete: 'cascade' });
      }
    }
});
  return BillingData;
};
