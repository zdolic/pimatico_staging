
//SALVE CAN HAVE ONLY ONE ENTRY.... FOR HIMSELF
module.exports = function(sequelize, DataTypes) {
	var Slaves = sequelize.define('Slaves', {
    uuid: DataTypes.STRING, //this one holds unique identifier of the remote device...
    name: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        },
    additionalInfo: DataTypes.TEXT,
    codeVersion: {
            type: DataTypes.REAL,
            defaultValue: 0,
            allowNull: false
        },
    enabled: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
    updateRequired: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
    isOnline: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
  }, {
    classMethods: {
      associate: function(models) {
        Slaves.belongsTo(models.User, { onDelete: 'cascade' });
      },
      publicAttributes: function() {
        return ['uuid', 'name', 'additionalInfo', 'codeVersion', 'updateRequired', 'isOnline'];
      }
    },
    instanceMethods: {
      generateKey: function() {
                return "key_"+Math.round((new Date().valueOf() * Math.random())) + '';
            }
    }
});
  return Slaves;
};
