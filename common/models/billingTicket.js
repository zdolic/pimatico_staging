
module.exports = function(sequelize, DataTypes) {
	var BillingTicket = sequelize.define('BillingTicket', {
	key: DataTypes.INTEGER,
  amount: DataTypes.FLOAT,

  status: { //0-open, 1-paid or closed, 2-canceled, 3-gratis
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  }, 
  optData: DataTypes.STRING(500)  //json type but with 500 leters limit
  }, 
  {
    indexes: [{
      name: 'billingTickets_key_index',
      method: 'BTREE',
      fields: ["key"]
    }],
    classMethods: {
      associate: function(models) {
     //   BillingTicket.hasMany(models.TimePlan, { onDelete: 'cascade' });
      }
    }
});
  return BillingTicket;
};
