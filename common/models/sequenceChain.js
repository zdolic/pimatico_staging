
module.exports = function(sequelize, DataTypes) {
	var SequenceChain = sequelize.define('SequenceChain', {
   	name: DataTypes.STRING,
    executing: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }
  });
  return SequenceChain;
};
