
module.exports = function(sequelize, DataTypes) {
	var DevicePlan = sequelize.define('DevicePlan', {
   /* recurring: { //0 - none, 1 day, 2 week, 3 month, 4 year
      type: DataTypes.INTEGER,
      defaultValue: 0
    },*/
   // startDateTimestamp: DataTypes.INTEGER,
    actionParams: DataTypes.STRING, //json object for specciffic type of output device
    enabled: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
    executionInProgress: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
       }
  }, {
    classMethods: {
      associate: function(models) {
        DevicePlan.belongsTo(models.Device, { onDelete: 'cascade' });
      }
    }
});
  return DevicePlan;
};
