
module.exports = function(sequelize, DataTypes) {
	var SequenceOutput = sequelize.define('SequenceOutput', {
    name: DataTypes.STRING,
    signalProperties: DataTypes.STRING,
    interruptHandlerFunction: DataTypes.TEXT,
    type: { //0 - fiksed output, 1 - code execution with outputs
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false
    },
    code: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    executing: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
       }
  }, {
    classMethods: {
      associate: function(models) {
        SequenceOutput.belongsTo(models.Device, { onDelete: 'cascade' }); //device type must be output.... make validation on DB level
      }
    }
});
  return SequenceOutput;
};
