
module.exports = function(sequelize, DataTypes) {
	var TimePlan = sequelize.define('TimePlan', {
   /* recurring: { //0 - none, 1 day, 2 week, 3 month, 4 year
      type: DataTypes.INTEGER,
      defaultValue: 0
    },*/
   // startDateTimestamp: DataTypes.INTEGER,
   type: {
            type: DataTypes.INTEGER,
            defaultValue: 0, //0 is plan for device, 1 is plan for sequences, 3 is plan for AccessControll
            allowNull: false
        },
    actionParams: DataTypes.STRING, //json object for specciffic type of output device
    enabled: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
    executionInProgress: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
       },
       isTemp:{
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: true
       }
  }, {
    classMethods: {
      associate: function(models) {
        TimePlan.belongsTo(models.Device, { onDelete: 'cascade' });
      }
    }
});
  return TimePlan;
};
