
module.exports = function(sequelize, DataTypes) {
	var Room = sequelize.define('Room', {
	name: { type: DataTypes.STRING, allowNull: false }
  }, {
    classMethods: {
      associate: function(models) {
        Room.belongsTo(models.User, {as: 'createdBy', foreignKey: 'createdById'});
      }
    }
});
  return Room;
};
