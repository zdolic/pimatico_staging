//here we will keep all of the configuration data ... for aplication and for specific modules..

module.exports = function(sequelize, DataTypes) {
	var JSONData = sequelize.define('JSONData', {
	key: DataTypes.STRING,
  data: DataTypes.TEXT //json type
  },  {
    indexes: [{
      name: 'jsondata_key_index',
      method: 'BTREE',
      fields: ["key"]
    }],
    classMethods: {
      associate: function(models) {}
    }
});
  return JSONData;
};
