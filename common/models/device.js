
module.exports = function(sequelize, DataTypes) {
	var Device = sequelize.define('Device', {
	name: DataTypes.STRING,
	gpio: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  }, //gpio pin adress
	type: DataTypes.INTEGER, // 1 -output with sw toggle switch, 2- (output reserved for PWM or Analog), 3- input interrupt with sw interrupt, 4- digital input state, 5- sanalog input, 6 - Sensor scaled readout
	defaultState: { // true is HIGH and false is LOW
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
  currentState: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
  },
  enableUIInteraction: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
  },
  invertUILayout: { //this will invert toggle button functionalities in UI layout if nessesary... should be in jsonPArams
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: true
  },
  lockedBySequence: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
  },
  mode: { //1-manual, 2-automatic
            type: DataTypes.INTEGER,
            defaultValue: 2,
            allowNull: false
        },
labelData: { //some devices support custom labels for different states. Expected format is stringified JSON.
  type: DataTypes.STRING(500),
  defaultValue: null,
  allowNull: true
},
style: { //styling information for future use...
  type: DataTypes.STRING(500),
  defaultValue: null,
  allowNull: true   
}

  }, {
    classMethods: {
      associate: function(models) {
        Device.belongsTo(models.Room, { onDelete: 'cascade' });
        Device.hasMany(models.TimePlan, { onDelete: 'cascade' });
        Device.belongsTo(models.Peripheral, { onDelete: 'cascade' });
      }
    }
});
  return Device;
};
