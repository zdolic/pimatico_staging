
module.exports = function(sequelize, DataTypes) {
	var SequenceChainLink = sequelize.define('SequenceChainLink', {
    linkOrder: DataTypes.INTEGER,
    linkOffsetMS: DataTypes.INTEGER,
    leadingOffsetExec: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
       }
  }, {
    classMethods: {
      associate: function(models) {
        SequenceChainLink.belongsTo(models.SequenceChain, { onDelete: 'cascade' }); 
        SequenceChainLink.belongsTo(models.SequenceOutput, { onDelete: 'cascade' }); 
      }
    }
});
  return SequenceChainLink;
};
