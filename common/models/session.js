module.exports = function(sequelize, DataTypes) {
  var Session = sequelize.define('Session', {
    sid: {type: DataTypes.STRING(255), primaryKey: true},
    expires: DataTypes.DATE,
    data: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  });
  return Session;
};