//this model preresent data extensions for some devices of type 5 and larger
module.exports = function(sequelize, DataTypes) {
	var DeviceSensor = sequelize.define('DeviceSensor', {

  address:{ //adressing informations.....
    type: DataTypes.STRING(500),
    defaultValue: null,
    allowNull: true    
  },
  currentValue: {
    type: DataTypes.FLOAT,
    defaultValue: null,
    allowNull: true
  },
  scType: { //scale type.... 1 - digital or 2 - analog :)
    type: DataTypes.INTEGER,
    defaultValue: 1,
    allowNull: false //because some devices have no scale...
  },
  scMin: {
    type: DataTypes.INTEGER,
    defaultValue: -100,
    allowNull: true
  },
  scMax: {
    type: DataTypes.INTEGER,
    defaultValue: 100,
    allowNull: true
  },
  scStep:{
    type: DataTypes.FLOAT,
    defaultValue: 1,
    allowNull: true

  },
  scStyle:{ //gauge styling informations...
    type: DataTypes.STRING(500),
    defaultValue: null,
    allowNull: true    
  },
  refreshMs: { //refresh in milliseconds
    type: DataTypes.INTEGER,
    defaultValue: 1000,
    allowNull: false
  },
  }, {
    classMethods: {
      associate: function(models) {
        DeviceSensor.belongsTo(models.Device, { onDelete: 'cascade' }); //there should be only one deviceSensor for sindle Device entity. 1:1 relationship
      }
    }
});
  return DeviceSensor;
};
