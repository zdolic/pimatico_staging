module.exports = function(sequelize, DataTypes) {
	var MasterNotification = sequelize.define('MasterNotification', {
	key: DataTypes.STRING,
  color: DataTypes.STRING,
	title: DataTypes.STRING,
  message: DataTypes.TEXT, //json type
  isActive: { //inactive messages will newer go to users unread notification queue
            type: DataTypes.BOOLEAN,
            defaultValue: true,
            allowNull: false
       } 
  }, {
    classMethods: {
      associate: function(models) {
      	 MasterNotification.belongsToMany(models.User, { through: 'MasterNotificationRead' });
      }
    }
});
  return MasterNotification;
};
