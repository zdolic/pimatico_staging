//keeps random application data needed for modules...
//for example, it keeps card and user informations for wiegand AccessControll module....
module.exports = function(sequelize, DataTypes) {
	var Dictionary = sequelize.define('Dictionary', {
	key: {type: DataTypes.STRING, unique: true},
  type: DataTypes.STRING, //null for undefined...
  data: DataTypes.TEXT //json type
  },  {
    indexes: [{
      name: 'Dictionary_key_index',
      method: 'BTREE',
      fields: ["key"]
    }],
    classMethods: {
      associate: function(models) {}
    }
});
  return Dictionary;
};
