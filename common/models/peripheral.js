
module.exports = function(sequelize, DataTypes) {
	var Peripheral = sequelize.define('Peripheral', {
	uid: DataTypes.INTEGER,
  isInitilized: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
    allowNull: false
  },
  configOptions: DataTypes.STRING(1000)  //json type but with 1000 leters limit
  }, 
  {
    classMethods: {
      associate: function(models) {
     //   BillingTicket.hasMany(models.TimePlan, { onDelete: 'cascade' });
      }
    }
});
  return Peripheral;
};
