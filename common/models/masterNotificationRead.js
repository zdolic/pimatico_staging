module.exports = function(sequelize, DataTypes) {
	var MasterNotificationRead = sequelize.define('MasterNotificationRead', {
	status: {
            type: DataTypes.INTEGER,
            defaultValue: 0, //1 - unread / 0 - read / 2 - postponed 
            allowNull: false
        },
  }, {
    classMethods: {
      associate: function(models) {
        MasterNotificationRead.belongsTo(models.MasterNotification);
      	MasterNotificationRead.belongsTo(models.User, { onDelete: 'cascade' });
      }
    }
});
  return MasterNotificationRead;
};
