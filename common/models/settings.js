module.exports = function(sequelize, DataTypes) {
	var Settings = sequelize.define('Settings', {
  type: { //0 - master/global, 1 - slave/local, 2 - everybody
            type: DataTypes.INTEGER,
            defaultValue: 1, 
            allowNull: false
          },
	prop_name: DataTypes.STRING,
  prop_value: DataTypes.STRING //json type
  }, {
    classMethods: {
      associate: function(models) {}
    }
});
  return Settings;
};
