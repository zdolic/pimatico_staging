'use strict';

var fs        = require('fs'),
  path      = require('path'),
  Sequelize = require('sequelize'),
  lodash    = require('lodash'),
  db        = {},
  dbConfig = require('../../dbConfig');
 // config    = require('../../config');

//var sequelize = new Sequelize(config.database, config.username, config.password, { dialect: config.dialect, logging: false, define: { charset: 'utf8', collate: 'utf8_general_ci' }, storage: config.storage });
var sequelize = new Sequelize({dialect: dbConfig.dialect, storage: dbConfig.storagePath, logging:false });

//console.log(":::::::::::"+__dirname, config.models_path);
//console.log("Storage path and filename: ",config.storage);
fs
  .readdirSync(dbConfig.models_path)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(dbConfig.models_path, file));
    db[model.name] = model;
  });
 
Object.keys(db).forEach(function(modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

module.exports = lodash.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db);

