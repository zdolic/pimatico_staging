
module.exports = function(sequelize, DataTypes) {
	var SequenceEvent = sequelize.define('SequenceEvent', {
    eventParams: {
            type: DataTypes.STRING,
            defaultValue: null, 
            allowNull: true
        },
        type: {
            type: DataTypes.INTEGER,
          //  defaultValue: 0, // 0- GPIO, 1-scheduled, 2 - boot events, 3- API calls, 4- Interrupt Time Out
            allowNull: false
        }
  }, {
    classMethods: {
      associate: function(models) {
        SequenceEvent.belongsTo(models.SequenceOutput, { onDelete: 'cascade' }); 
        SequenceEvent.belongsTo(models.Device, { onDelete: 'cascade' }); 
        SequenceEvent.belongsTo(models.SequenceChain, { onDelete: 'cascade' }); 
      }
    }
});
  return SequenceEvent;
};
