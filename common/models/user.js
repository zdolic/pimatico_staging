var crypto = require('crypto');

module.exports = function(sequelize, DataTypes){
    var User = sequelize.define('User', {
        name: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        email: {type: DataTypes.STRING, unique: true},
        hashed_password: DataTypes.STRING,
        salt: DataTypes.STRING,
        phone: DataTypes.STRING,
        avatar: DataTypes.STRING,
        enabled: { type: DataTypes.BOOLEAN, defaultValue: true},
        registered: { type: DataTypes.BOOLEAN, defaultValue: false},
        verified: {type: DataTypes.BOOLEAN, defaultValue: false},
        verificationLink: DataTypes.STRING,
        accountType: { type: DataTypes.INTEGER, defaultValue: 0 }, // 0 - local registered user, 1- cloud registered user
        role: { type: DataTypes.INTEGER, defaultValue: 0 } // 0 - regullar SLAVE user, 1 - MASTER administrator, used only on MASTER instance
    }, {
        classMethods: {
            publicAttributes: function() {
                return ['id', 'email', 'name', 'phone', 'avatar', 'enabled', 'accountType'];
            }
        },
        instanceMethods: {
           /* setPassword: function(password) {
                this.salt = this.makeSalt();
                this.hashed_password = this.encryptPassword(password);
            },*/
            setPasswordWithExistingSalt: function(password){
                this.hashed_password = this.encryptPassword(password);
            },
            encryptPassword: function(password) {
                if (!password) {return '';}
                return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
            },
            authenticate: function(plainText) {
                return this.encryptPassword(plainText) === this.hashed_password;
            }/*,
            makeSalt: function() {
                return Math.round((new Date().valueOf() * Math.random())) + '';
            }*/
        }
    });
    return User;
};