
module.exports.model2endpointMap = {
	Device : "/devices",
	Room: "/rooms",
	TimePlan: "/plans",
	MasterNotification : "/masterNotifications",
	SequenceOutput: "/sequenceOutputs",
	SequenceEvent: "/sequenceEvents",
	SequenceChainLink: "/sequenceChainLinks",
	SequenceChain: "/sequenceChains",
	JSONData: "/jsonData",
	Peripheral: "/peripherals",
	DeviceSensor: "/deviceSensor"
}; //here is collection of endpoints and db model mappings. endpoint is also namespace for localsocket....


module.exports.clientSideSocketEvent_4Master = {
	piemitbroadcast: "piemitbroadcast",
	slaveDisconnected: "slaveDisconnected",
	slaveConnected : "slaveConnected"
};


module.exports.commonClientSideSocketEvent = {
	updateRequired: "updateRequired"

};

module.exports.JSONSettingsKeys = {
	masterLocationAddress: "masterLocationAddress", //holds address of cloud instance...
	managedApiOutputs : "managedApiOutputs", //{[]} //small footprint, hold DeviceId in arra
	managedApiSequenceEvents : "managedApiSequenceEvents", // {[]} //small footprint
	extensionSettingsKey : "extensionSettingsKey", //{[{modulepath: string, enabled: true/false }], ...}
	advancedUI: "advancedUI" //true or false ...initialy true...
};

module.exports.deviceTypes = {
	1: "OutputPinWithUIToggleSwitch",
	2: "OutputPinWithPushButtonSwitch",
	3: "DigitalInputInterrupt",
	4: "DigitalInputState" 
};
