var enums = require("../../config/enums");
var _ = require("lodash");
var Promise = require("bluebird");
var JSONDataController = require('../db/JSONDataController.js');

module.exports.initDbSettingBuild = function(){ //cal this only when creating database....
	return new Promise(function(resolve, reject){
		var allEnumKeyValues = [];

		_.forIn(enums.JSONSettingsKeys, (value, key)=>{
			allEnumKeyValues.push(value)
		});

		var rebuildKeys = function(index){
			JSONDataController.getDataByKey_FindOrCreate(allEnumKeyValues[index]).then(()=>{
				if(index<allEnumKeyValues.length-1){
					rebuildKeys(index+1);
				} else {
					resolve(true);
				}
			});
		}
		rebuildKeys(0);
	});
};

module.exports.getSettings = function(key){
	return JSONDataController.getDataByKey_FindOrCreate(key);
};

module.exports.setDataForKey = function(key, data){
	return JSONDataController.setDataForKey(key, data);
};

module.exports.setSettings = function(key, data){
	return JSONDataController.setDataForKey(key, data);
};

module.exports.SettingsKeys = enums.JSONSettingsKeys;