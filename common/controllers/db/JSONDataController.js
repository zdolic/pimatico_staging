var db = require('../../../common/models');
var Promise = require("bluebird");
var _ = require("lodash");

module.exports.getDataByKey_FindOrCreate = function(key){
	return new Promise(function(resolve, reject){
		db.JSONData.findOrCreate({where: {key:key}, defaults:{ key: key, data:null }}).spread(function(createdData, created){
			
			if(createdData.dataValues.data){
				var result = createdData.dataValues.data;
				if(_.isString(createdData.dataValues.data)){
					//console.log(createdData.dataValues.data);
					result = JSON.parse(createdData.dataValues.data);
				} 
				resolve(result);
			} else {
				resolve(null);
			}
			
		}).catch(err=>reject(err));
	})
};

module.exports.getDataByKey = function(key){
	return new Promise(function(resolve, reject){
		db.JSONData.find({where: {key:key}}).then(function(record){
			if(record){
				var result = JSON.parse(record.dataValues.data);
				resolve(result);
			} else {
				reject();//reject if no data...
			}
			
		}).catch(err=>reject(err));
	});
};

module.exports.getIdByKey = function(key){
	return new Promise(function(resolve, reject){
		db.JSONData.find({where: {key:key}}).then(function(record){
			if(record){
				var result = JSON.parse(record.dataValues.id);
				resolve(result);
			} else {
				reject();//reject if no data...
			}
			
		}).catch(err=>reject(err));
	});
};

module.exports.setDataForKey = function(key, data){
	return new Promise(function(resolve, reject){
		db.JSONData.find({where: {key:key} }).then((item)=>{
			if(_.isString(data)){
				item.updateAttributes({data:data}).then((it)=>{
					var result = JSON.parse(it.dataValues.data);
					resolve(result);	
				}).catch(err=>reject(err));
			} else {
				item.updateAttributes({data:JSON.stringify(data)}).then((it)=>{
					var result = JSON.parse(it.dataValues.data);
					resolve(result);	
				}).catch(err=>reject(err));
			}
		}).catch(err=>reject(err));
	});
}